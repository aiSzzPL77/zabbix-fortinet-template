# Zabbix Fortigate template

Just an up to date version of the excellent [Fortigate SNMP template](https://share.zabbix.com/network_devices/fortigate/fortigate-snmp-template) with
working network interface monitoring.

Monitors: Serial N., OS version, Connection num, CPU%, RAM%, Disk (Total and Used), Interface data (link and speed).

Graph for everything.

## Authors

* Leonardo Nascimento da Silva
* [Andrea Durante](https://share.zabbix.com/owner/g_118177391363399555513)
* [Lorenzo Milesi](https://lorenzo.mile.si)
